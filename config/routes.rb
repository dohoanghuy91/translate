Rails.application.routes.draw do

  root 'homes#index'
  get '/:locale' => 'homes#index'
  scope "(:locale)"  do
    resources :homes
  end
end
